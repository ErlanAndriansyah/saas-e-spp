//Fungsi untuk autosuggest
function suggest(src)
{
	var page    = 'check-login.php';
	if(src.length>=2){
		var loading = '<p align="center">Loading ...</p>';
		showStuff('suggest');
		$('#check-login').html(loading);
		$.ajax({
			url: page,
			data : 'src='+src,
			type: "post", 
			dataType: "html",
			timeout: 10000,
			success: function(response){
				$('#check-login').html(response);
			}
		});
	}
}

//Fungsi untuk memilih kota dan memasukkannya pada input text
function pilih_nama(Nama)
{
	$('#username').val(Nama);
}

//menampilkan form div
function showStuff(id) {
	document.getElementById(id).style.display = 'block';
}
//menyembunyikan form
function hideStuff(id) {
	document.getElementById(id).style.display = 'none';
}