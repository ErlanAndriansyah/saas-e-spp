-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2019 at 04:06 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spponline_v2`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `username` varchar(10) NOT NULL,
  `password` varchar(16) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `NIP` int(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `password`, `nama`, `NIP`) VALUES
('admin1', '12345', 'Sueb', 1234),
('admin2', '123456', 'jojo', 1235);

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(5) NOT NULL,
  `kelas` varchar(10) NOT NULL,
  `wakel` varchar(25) NOT NULL,
  `nip_wakel` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `kelas`, `wakel`, `nip_wakel`) VALUES
(1, 'XI SIJA A', 'Ibu Novi', '12321318');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `NIS` int(20) NOT NULL,
  `Id_kelas` int(5) NOT NULL,
  `Tahun_ajaran` varchar(25) NOT NULL,
  `Nama` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`NIS`, `Id_kelas`, `Tahun_ajaran`, `Nama`) VALUES
(12, 1, '2019/2020', 'Afif Lutfianto'),
(14, 1, '2019/2020', 'Aini Nurul Azizah'),
(13, 1, '2019/2020', 'Alifya Difa Afasyah'),
(15, 1, '2019/2020', 'Dewi Nur setiawati'),
(15, 1, '2019/2020', 'Dimas Azmi Haikal'),
(10, 1, '2019/2020', 'Erlan Andriansyah'),
(16, 1, '2019/2020', 'Eva Kristina'),
(11, 1, '2019/2020', 'Maghfiratunnisa');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `Nama` varchar(25) NOT NULL,
  `id` int(25) NOT NULL,
  `NoResi` varchar(25) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `tgl_input` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `saldo` int(25) NOT NULL,
  `jumlah_terbayar` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`Nama`, `id`, `NoResi`, `tgl_bayar`, `tgl_input`, `saldo`, `jumlah_terbayar`) VALUES
('Erlan Andriansyah', 2, '101010101', '2019-02-01', '2019-02-28 04:52:58', 200000, 200000),
('Maghfiratunnisa', 3, '1232312313', '2019-02-09', '2019-02-28 06:34:28', 300000, 0),
('Erlan Andriansyah', 4, '12312321324', '2019-02-19', '2019-02-28 07:09:51', 100000, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`NIP`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`Nama`),
  ADD KEY `kelas` (`Id_kelas`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Nama` (`Nama`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `siswa`
--
ALTER TABLE `siswa`
  ADD CONSTRAINT `kelas` FOREIGN KEY (`Id_kelas`) REFERENCES `kelas` (`id_kelas`);

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`Nama`) REFERENCES `siswa` (`Nama`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
