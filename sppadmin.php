<?php
include "config.php";
session_start();
if (!isset($_SESSION['username'])){
header ("location:login2.php");
}
$nama = $_SESSION;
?>
<!DOCTYPE html>


<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>e-SPP || Pengecekan SPP dan DSP</title>

  
    <link href="css/bootstrap.min.css" rel="stylesheet">

    
    <link href="css/custom.css" rel="stylesheet">

    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    
</head>

<body>

    
    <nav id="siteNav" class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Logo and responsive toggle -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                	<span class="glyphicon glyphicon-list-alt"></span> 
                	e-SPP
                </a>
            </div>
            <!-- Navbar links -->
            <div class="collapse navbar-collapse" id="navbar">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active">
                        <a href="index.php">Home</a>
                    </li>
                    <li>
                        <a href="logout.php">Logout</a>
                    </li>
					
                    <li>
                       
                    </li>
                </ul>
                
            </div>
        </div>
    </nav>

	

	
     <section class="content content-3">
        <div class="container">
			<h2 class="section-header"><span class="glyphicon glyphicon-credit-card text-primary"></span><br>Masukkan data pembayaran SPP</h2>
			<p class="lead text-muted"></p> 
                                
            </div>
        </div>
    </section>
    <form class="form-horizontal-xl" action="inputspp.php" method="post">
  <div class="form-group">
    <label class="control-label col-sm-2" for="nama">Nama Siswa</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="nama" name="namasiswa" placeholder="Masukkan Nama Siswa">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="nomor">Nomor Induk Siswa</label>
    <div class="col-sm-10"> 
      <input type="text" class="form-control" name="nis"id="nomor" placeholder="Masukkan NIS">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="resi">No.Resi</label>
    <div class="col-sm-10"> 
      <input type="text" class="form-control" name="noresi"id="resi" placeholder="Masukkan No.Resi">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="nomor">Jumlah </label>
    <div class="col-sm-10"> 
      <input type="text" class="form-control" id="nomor" name="duit"placeholder="Masukkan Jumlah Uang">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="tanggal">Tanggal Bayar</label>
    <div class="col-sm-10"> 
      <input type="date" class="form-control" id="tanggal" name="tgl">
    </div>
  </div>

  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" value="submit"class="btn btn-success btn-block">Submit</button>
    </div>
  </div>
</form>
    
	
        
</div>
        	</div>
        </div>
         	
        
        <div class="small-print">
        	<div class="container">

        	</div>
        </div>
        
    </footer>

    <!-- jQuery -->
    <script src="js/jquery-1.11.3.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>
    
    <!-- Custom Javascript -->
    <script src="js/custom.js"></script>

</body>

</html>
