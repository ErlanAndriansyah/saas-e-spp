<?php
include "config.php";
session_start();
if (!isset($_SESSION['Nama'])){
header ("location:login.php");
}
$nama = $_SESSION;
?>
<!DOCTYPE html>


<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>e-SPP || Pengecekan SPP dan DSP</title>

  
    <link href="css/bootstrap.min.css" rel="stylesheet">

    
    <link href="css/custom.css" rel="stylesheet">

    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    
</head>

<body>

    
    <nav id="siteNav" class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Logo and responsive toggle -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                	<span class="glyphicon glyphicon-list-alt"></span> 
                	e-SPP
                </a>
            </div>
            <!-- Navbar links -->
            <div class="collapse navbar-collapse" id="navbar">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active">
                        <a href="index.php">Home</a>
                    </li>
                    <li>
                        <a href="logout.php">Logout</a>
                    </li>
					
                    <li>
                       
                    </li>
                </ul>
                
            </div>
        </div>
    </nav>

	

	
     <section class="content content-3">
        <div class="container">
			<h2 class="section-header"><span class="glyphicon glyphicon-pushpin text-primary"></span><br>Data Pembayaran DSP <?php echo $_SESSION["Nama"]?></h2>
			<p class="lead text-muted"></p> 
                                
            </div>
        </div>
    </section>
    
	
        <div class="container">
            <table class="table table-hover table-striped" >
        

        	
    <thead>
      <tr>
      
        <th><center>Tanggal Bayar</th>
        <th><center>Jumlah Bayar</th>
        <th><center>No.Pembayaran</th>
        <th><center>Sisa bayar</th>
   
      </tr>
    </thead>
    <?php  

$hasil=$dbconnect->query('SELECT * FROM siswa INNER JOIN transaksi_dsp USING (Nama) where siswa.Nama = "'.$_SESSION["Nama"].'"') ;


while ($data = mysqli_fetch_array($hasil)){
        echo"<tbody>";
            echo"<tr>";
                echo"<td><center>".$data['tgl_input']."</td>";
                echo"<td><center>Rp.".$data['saldo']."</td>";
                echo"<td><center>".$data['noresi']."</td>";
           
                
            echo"</tr>";
        echo"</tbody>";
    }
   ?>
   <?php
    $total=$dbconnect->query('SELECT SUM(saldo) FROM transaksi where Nama= "'.$_SESSION["Nama"].'"') ;
    while ($data = mysqli_fetch_array($total)){
    }
    ?>

    
    </table>
</div>

        	</div>
        </div>
         	
        
        <div class="small-print">
        	<div class="container">

        	</div>
        </div>
        
    </footer>

    <!-- jQuery -->
    <script src="js/jquery-1.11.3.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>
    
    <!-- Custom Javascript -->
    <script src="js/custom.js"></script>

</body>

</html>
