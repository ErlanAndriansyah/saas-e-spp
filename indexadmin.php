<?php
include "config.php";
session_start();
if (!isset($_SESSION['username'])){
header ("location:login2.php");
}

?>
<!DOCTYPE html>


<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>e-SPP || Pengecekan SPP dan DSP</title>

  
    <link href="css/bootstrap.min.css" rel="stylesheet">

    
    <link href="css/custom.css" rel="stylesheet">

    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    
</head>

<body>

    
    <nav id="siteNav" class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Logo and responsive toggle -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                	<span class="glyphicon glyphicon-list-alt"></span> 
                	e-SPP
            </div>
            <!-- Navbar links -->
            <div class="collapse navbar-collapse" id="navbar">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active">
                        <a href="#">Home</a>
                    </li>
                    <li>
                        <a href="logout.php">Logout</a>
                    </li>
					
                    <li>
                       
                    </li>
                </ul>
                
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container -->
    </nav>

	<!-- Header -->
  
	<!-- Intro Section -->
    <section class="intro">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                	<span class="glyphicon glyphicon-briefcase" style="font-size: 60px"></span>
                    <h2 class="section-heading">Hai! Admin </h2>
                    <p class="text-light">Selamat bertugas!</p>
                </div>
            </div>
        </div>
    </section>

	<!-- Content 1 -->
    
	<!-- /.container-fluid -->

	<!-- Content 3 -->
     <section class="content content-3">
        <div class="container">
			<h2 class="section-header"><span class="glyphicon glyphicon-check "></span><br>Pengisian Data SPP dan DSP</h2>
			<p class="lead text-muted"></p> 
            <div class="btn-group">
  <a href="sppadmin.php" button type="button" class="btn btn-primary">SPP</a>
 <a href="dspadmin.php" button type="button"  class="btn btn-primary">DSP </a>                   
            </div>
        </div>
    </section>
    
	<!-- Footer -->
    <footer class="page-footer">
    
    	<!-- Contact Us -->
        <div class="contact">
        	<div class="container">
				<h2 class="section-heading">Pengecekan Data</h2>
				<p>Cek data dari seluruh siswa SMKN 1 Cimahi<br> </p>
			
        	</div>
        </div>
        	
        <!-- Copyright etc -->
        <div class="small-print">
        	<div class="container">
        		<p>Copyright &copy; Erlan 2019</p>
        	</div>
        </div>
        
    </footer>
     <!-- jQuery -->
     <script src="js/jquery-1.11.3.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="js/jquery.easing.min.js"></script>

<!-- Custom Javascript -->
<script src="js/custom.js"></script>

</body>

anda admin, <a href="logout.php">Logout</a>