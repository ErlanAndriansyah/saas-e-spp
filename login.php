<?php
include "config.php";
session_start();
if (isset($_SESSION['Nama'])){
header ("location:index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Login || Pengecekan SPP dan DSP Online</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>



    <div class="kotak_login">
        <p class="tulisan_login">Silahkan login</p>

        <form method="post" name="login" action="check-login.php">
            <label>Nama</label>
            <input type="text" name="Nama" id="Nama" class="form_login" onkeypress="check-login(this.value);" placeholder="Nama Siswa">
            <div id="suggest"></div>
            <label>NIS</label>
            <input type="password" name="NIS" class="form_login" placeholder="NIS">

            <input type="submit" class="tombol_login" value="LOGIN">

            <br/>
            <br/>
            <center>
                <a class="link" href="welcome.php">kembali</a>
            </center>
        </form>
        
    </div>

    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/prmajax.js"></script>
</body>
</html>